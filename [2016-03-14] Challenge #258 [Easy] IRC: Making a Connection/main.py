#!/usr/bin/env python3

"""
 Your challenge today will be to communicate with the freenode IRC server.
 This will consist of opening a TCP socket to freenode and sending two protocol
 messages to initiate the connection.

 Challenge link: https://www.reddit.com/r/dailyprogrammer/comments/4ad23z/20160314_challenge_258_easy_irc_making_a/
"""


def main():
    import socket

    encoding = 'utf8'
    program_input = 'chat.freenode.net:6667\npow\npow_thegreat\nPow Pow'
    server, nickname, username, realname = program_input.splitlines()
    server = server.split(':')
    server[1] = int(server[1])

    connection = open_socket(socket, tuple(server))

    connection.send(bytes('NICK {0}\r\n'.format(nickname), encoding))
    connection.send(bytes('USER {0} {1} {2} :{3}\r\n'.format(nickname, 0, '*', realname), encoding))

    print('message sent')

    ping_pong_with_server(connection)

    connection.close()


def open_socket(socket, server):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(server)

    print('connected', server)

    return s


def ping_pong_with_server(connection):
    buffer = ''
    encoding = 'utf8'

    while True:
        if '\r\n' not in buffer:
            buffer += connection.recv(512).decode(encoding)

        line, buffer = buffer.split('\r\n', 1)
        print(line)

        line = line.split()
        if line[0] == 'PING':
            connection.send(bytes('PONG {0}\r\n'.format(line[1])), encoding)


if __name__ == '__main__':
    main()
