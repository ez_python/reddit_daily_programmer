#!/usr/bin/env python3

def grab(argv):

    if len(argv) < 2:
        print('This program needs two inputs: a line of text and a file to read.')
        print(argv)
        print(len(argv))
        exit()

    script, word, file_name = argv

    print('Word: {}'.format(word))
    print('Name of file: {}\n'.format(file_name))

    with open(file_name, 'r') as f:
        for i, line in enumerate(f, start=1):
            if word.upper() in line.upper():
                print('{} {}'.format(i, line.strip()))


if __name__ == '__main__':
    from sys import argv

    grab(argv)
