#!/usr/bin/env python3


def text_files_in_current_folder():
    from os import listdir

    return [f for f in listdir('.') if '.txt' in f]


def grab(word, file):
    """

    :type word: String
    :type file: Obj
    """
    printed_something = 0
    with open(file, 'r') as f:
        for i, line in enumerate(f, start=1):
            if word.upper() in line.upper():
                print('{} {}'.format(i, line.strip()))
                printed_something = 1

    if printed_something:
        print('Found in {}'.format(file))


def prepare(args):
    # No file specified. Look in entire current folder
    if len(args) <= 2:
        script, word = args
        for file in text_files_in_current_folder():
            grab(word, file)

    else:
        script, word, file = args
        grab(word, file)


if __name__ == '__main__':
    from sys import argv

    prepare(argv)
